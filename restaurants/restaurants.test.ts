import 'jest'
import * as request from 'supertest'

const address: string = (<any>global).address
const auth: string = (<any>global).auth

test('get /restaurants', ()=>{
	return request(address)
	.get('/restaurants')
	.then(response=>{
		expect(response.status).toBe(200)
		expect(response.body.items).toBeInstanceOf(Array)
	})
	.catch(fail)
})

test('post /restaurants', ()=>{
  return request(address)
		 .post('/restaurants')
		 .set('Authorization', auth)
     .send({
     	name: 'Trogois',
     	menu: [{name: 'agua', price: 5}]
     })
     .then(response=>{
      expect(response.status).toBe(200)
      expect(response.body._id).toBeDefined()
     }).catch(fail)
})

test('put /restaurants', ()=>{
	return request(address)
	.post('/restaurants')
	.set('Authorization', auth)
	.send({
		name: 'TT burguer'
	})
	.then(response=> request(address)
		.put('/restaurants')
		.set('Authorization', auth)
		.send({
			name: 'TT burger - put'
		})
		.then(response =>(){
			    expect(response.status).toBe(200)
	        expect(response.body._id).toBeDefined()
	        expect(response.body.name).toBe('usuario4 - replace')

		})
		.catch(fail)
	)
})

test('delete /restaurants', ()=>{
	return request(address)
	.post('/restaurants')
	.set('Authorization', auth)
	.send({
		name: 'Big point'
	})
	.then(response=> request(address)
		.delete('/restaurants')
		.set('Authorization', auth)
		.send({})
		.then(response =>(){
				expect(response.status).toBe(204)
	      expect(response.body._id).toBeDefined()
		})
		.catch(fail)
	)
})
