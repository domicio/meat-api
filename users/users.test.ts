import 'jest'
import * as request from 'supertest'

const address: string = (<any>global).address
const auth: string = (<any>global).auth

test('get /users', ()=>{
  return request(address)
     .get('/users')
     .set('Authorization', auth)
     .then(response=>{
      expect(response.status).toBe(200)
      expect(response.body.items).toBeInstanceOf(Array)
     }).catch(fail)
})

test('post /users', ()=>{
  return request(address)
     .post('/users')
     .set('Authorization', auth)
     .send({
       name: 'usuario1',
       email: 'usuario1@email.com',
       password: '123456',
       cpf: '962.116.531-82'
     })
     .then(response=>{
        expect(response.status).toBe(200)
        expect(response.body._id).toBeDefined()
        expect(response.body.name).toBe('usuario1')
        expect(response.body.email).toBe('usuario1@email.com')
        expect(response.body.cpf).toBe('962.116.531-82')
        expect(response.body.password).toBeUndefined()
     }).catch(fail)
})

test('get /users/aaaaa - not found', ()=>{
    return request(address)
     .get('/users/aaaaa')
      .set('Authorization', auth)
     .then(response=>{
      expect(response.status).toBe(404)
     }).catch(fail)
})

test('patch /users/:id', ()=>{
  return request(address)
     .post('/users')
     .set('Authorization', auth)
     .send({
       name: 'usuario2',
       email: 'usuario2@email.com',
       password: '123456'
      })
     .then(response => request(address)
        .patch(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .send({
         name: 'usuario2 - patch'
        })
      )
     .then(response=>{
        expect(response.status).toBe(200)
        expect(response.body._id).toBeDefined()
        expect(response.body.name).toBe('usuario2 - patch')
        expect(response.body.email).toBe('usuario2@email.com')
        expect(response.body.password).toBeUndefined()
     })
     .catch(fail)
})

test('delete /users/:id', ()=>{
  return request(address)
     .post('/users')
     .set('Authorization', auth)
     .send({
       name: 'usuario3',
       email: 'usuario3@email.com',
       password: '123456'
      })
     .then(response => request(address)
        .delete(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .send({})
      )
     .then(response=>{
        //console.log(response)
        expect(response.status).toBe(204)
     })
     .catch(fail)
})

test('put /users/:id', ()=>{
  return request(address)
     .post('/users')
     .set('Authorization', auth)
     .send({
       name: 'usuario4',
       email: 'usuario4@email.com',
       password: '123456',
       cpf: '962.116.531-82'
      })
     .then(response => request(address)
        .put(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .send({
          name: 'usuario4 - replace',
           email: 'usuario4@email.com',
           password: '123456',
           cpf: '962.116.531-82'
        })
      )
     .then(response=>{
        expect(response.status).toBe(200)
        expect(response.body._id).toBeDefined()
        expect(response.body.name).toBe('usuario4 - replace')
     })
     .catch(fail)
})
