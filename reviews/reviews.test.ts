import 'jest'
import * as request from 'supertest'

const address: string =  (<any>global).address
const auth: string =  (<any>global).auth
let user_id : string
let restaurant_id : string

test('get /reviews', ()=>{
	return request(address)
	.get('/reviews')
	.then(response=>{
		expect(response.status).toBe(200)
		expect(response.body.items).toBeInstanceOf(Array)
	})
	.catch(fail)
})

test('post /reviews', ()=>{
	return request(address)
	.post('/users')
	.set('Authorization', auth)
	.send({
		name:'Domicio'
		email:'domiciom@gmail.com',
		password:'123456'
	})
	.then(response=>{
		user_id = response.body._id
		return request(address)
		.post('/restaurants')
		.set('Authorization', auth)
		.send({
			name: 'Big point'
		})
		.then(response=>{
			restaurant_id = response.body._id
			return request(address)
			.post('/reviews')
			.set('Authorization', auth)
			.send({
				date: '2019-10-11',
		  		rating: 5,
		  		comments: 'Teste',
		  		user: user_id,
		  		restaurant: restaurant_id
			})
			.then(response=>{
				expect(response.status).toBe(200)
				expect(response.body.date).toBe('2019-10-11T00:00:00.000Z')
				expect(response.body.rating).toBe(5)
				expect(response.body.comments).toBe('Teste')
			})
		})
	})
	.catch(fail)
})


// test('delete /reviews', ()=>{
// 	let user_id : string
// 	let restaurant_id : string
// 	return request(address)
// 	.post('/users')
// 	.set('Authorization', auth)
// 	.send({
// 		name:'Domicio2',
// 		email:'domiciom2@gmail.com',
// 		password:'123456'
// 	})
// 	.then(response=>{
// 		user_id = response.body._id
// 		return request(address)
// 		.post('/restaurants')
// 		.set('Authorization', auth)
// 		.send({
// 			name: 'Big point'
// 		})
// 		.then(response=>{
// 			restaurant_id = response.body._id
// 			return request(address)
// 			.post('/reviews')
// 			.set('Authorization', auth)
// 			.send({
// 				date: '2019-10-11',
// 		  		rating: 5,
// 		  		comments: 'Teste',
// 		  		user: user_id,
// 		  		restaurant: restaurant_id
// 			})
// 			.then(response=>{
// 				return request(address)
// 				.delete('/reviews')
// 				.set('Authorization', auth)
// 				.send({})
// 				.then(response=>{
// 					console.log(response)
// 					expect(response.status).toBe(204)
// 					expect(response.body._id).toBeDefined()
// 				})
// 				.catch(fail)
// 			})
// 		})
// 	})
// 	.catch(fail)
// })